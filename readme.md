# Chat bot Telegram



Projeto criado para o Tech Challeger Carrefour. 



O desafio era desenvolver uma solução que otimizasse a comunicação entre cliente e Carrefour através do Telegram.



Esta não é uma solução para um problema específico. Está mais para uma estrutura genérica que permite adicionar, novas formas de interação com os clientes. A aplicação possui um mecanismo de aprendizado nativo, que permite aprender com base no julgamento positivo (dos clientes) para soluções apresentadas para os questionamentos dos clientes. 



## Configurações inicias e execução da aplicação

É preciso criar os arquivos `.env` e `.env.test`, que devem ter o mesmo conteúdo do arquivo `.env.example`. 



Apenas para testar a aplicação basta preencher os campos dos dois novos arquivos com:

```env
telegramBotToken=SEU_TOKEN_TELEGRAM_BOT_AQUI
DB_HOST
DB_NAME
DB_USER
DB_PASS
DB_DIALECT=sqlite
DB_STORAGE=database/db.sqlite
DB_LOGGING
```

Em seguida, um arquivo chamado `db.sql` na pasta `database`. 



Para iniciar a aplicação execute: `npm start` ou  `yarn start`



## Solução de comunicação com os clientes

A solução apresenta as seguintes soluções para os clientes:



- Obter a linha digitável  de seu boleto

- Cadastrar-se para notificações diária de promoções

- Cancelar o cadastro de notificações de promoções diárias.
