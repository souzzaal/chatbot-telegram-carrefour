const factory = require('../../../database/factories')
const IntencoesService = require('../../../src/services/IntencoesService')

describe('IntencaoService', () => {

	it('deve calcular e ordenar por relevancia as intencoes de acordo com o texto digitado', async () => {

		const intencao1 = await factory.create('Intencao')
		const intencao2 = await factory.create('Intencao')

		const indicativos1 = ['termo1', 'termo2', 'termo3', 'termo4', 'termo5']
		const indicativos2 = ['termo3', 'termo6']

		indicativos1.forEach(async indicativo => {
			await factory.create('Indicativo', {
				nome: indicativo,
				intencao_id: intencao1.id
			})
		})

		indicativos2.forEach(async indicativo => {
			await factory.create('Indicativo', {
				nome: indicativo,
				intencao_id: intencao2.id
			})
		})

		const outrosIndicativos = await factory.createMany('Indicativo', 5)

		const resposta = await IntencoesService.identificaIntencao('Quero obter o termo1 termo2 do meu termo3')

		expect(true).toEqual(Array.isArray(resposta))
		expect(2).toEqual(resposta.length)

		expect(intencao1.nome).toEqual(resposta[0].nome)
		expect(3).toEqual(resposta[0].relevancia)

		expect(intencao2.nome).toEqual(resposta[1].nome)
		expect(1).toEqual(resposta[1].relevancia)

	})

})