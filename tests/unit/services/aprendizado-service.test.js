const aprendizadoService = require('../../../src/services/AprendizadoService')
const { Indicativo, Intencao, Mensagem } = require('../../../src/models')
const faker = require('faker/locale/pt_BR')
const { Op } = require("sequelize");

describe('AprendizadoService', () => {

	it('deve salvar os termos de pesquisa como uma string unica no banco de dados', async () => {
		const termos = faker.lorem.words(4).split(' ')

		await aprendizadoService.salvaTermosPesquisados(0, termos)

		const mensagem = await Mensagem.findOne({
			order: [['id', 'DESC']],
			limit: 1
		})

		expect(termos.join(' ')).toEqual(mensagem.termos)
	})

	it('deve atualizar a mensagem com a intencao do do cliente', async () => {

		const mensagem = await Mensagem.create({
			chat_id: 0,
			termos: faker.lorem.words(4)
		})

		const intencao = await Intencao.findOne({
			where: {
				nome: {
					[Op.not]: null
				}
			}
		})

		await aprendizadoService.salvaIntencao(0, intencao.nome)

		const mensagemAtualizada = await Mensagem.findOne({
			where: {
				id: mensagem.id
			}
		})

		expect(intencao.id).toEqual(mensagemAtualizada.intencao_id)
	})


	it('deve adicionar à Intencao os Indicativos que ainda não tem', async () => {

		const termos = faker.lorem.words(4)

		const intencao = await Intencao.findOne({
			where: {
				nome: {
					[Op.not]: null
				}
			}
		})

		const mensagem = await Mensagem.create({
			chat_id: 123,
			termos,
			intencao_id: intencao.id
		})

		await aprendizadoService.atualizaBaseDeConhecimento(123)

		const indicativos = await Indicativo.findAll({
			where: {
				intencao_id: intencao.id
			}
		})

		const indicativosNomes = indicativos.map(indicativo => indicativo.nome)

		termos.split(' ').forEach(termo => {
			expect(indicativosNomes.includes(termo)).toEqual(true)
		})

		const mensagemAtualizada = await Mensagem.findOne({
			where: {
				id: mensagem.id
			}
		})

		expect(1).toEqual(mensagemAtualizada.util)
	})


})