require('dotenv-safe').config({
	path: process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : '.env'
})

const bot = require('./services/BotService').getInstance();
const App = require('./app');


bot.onText(/\/start/, (msg) => {
	bot.sendMessage(msg.chat.id, "Tech Challenge Carrefour");
});

bot.on('message', async msg => {
	const app = new App(msg)
	app.inicio()

})

