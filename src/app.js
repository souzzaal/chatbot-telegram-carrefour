const aprendizadoService = require('./services/AprendizadoService');
const intencaoService = require('./services/IntencoesService');
const { Intencao } = require('./models')
const bot = require('./services/BotService');

class App {

	#intencaoManifestada = false
	#mensagemTelegram = null

	constructor(mensagemTelegram) {
		this.#mensagemTelegram = mensagemTelegram
	}

	async inicio() {
		const espacoEmBrancoRegex = /\s+/g
		if (this.#mensagemTelegram.text.startsWith('/') && !espacoEmBrancoRegex.test(this.#mensagemTelegram.text)) {
			await this.processaIntencao()
		}

		if (!this.#intencaoManifestada) {
			await this.identificaIntencao()
		}
	}

	async processaIntencao() {

		let intencaoSegura = '/' + this.#mensagemTelegram.text.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '')

		const intencao = await Intencao.findOne({
			where: {
				nome: intencaoSegura
			}
		})

		if (intencao) {
			intencaoService.processaIntencao(intencaoSegura, this.#mensagemTelegram)
			this.#intencaoManifestada = true
		}

	}

	async identificaIntencao() {
		const intencoes = await intencaoService.identificaIntencao(this.#mensagemTelegram.text)
		let keyboard = []
		if (intencoes.length) {

			aprendizadoService.salvaTermosPesquisados(this.#mensagemTelegram.chat.id, intencaoService.identificaTermosDePesquisa(this.#mensagemTelegram.text))

			intencoes.forEach(intencao => keyboard.push([intencao.nome]))

			const mensagem = [
				intencoes.length > 1 ? 'Aqui estão algumas opções pra você:' : 'Aqui está a opção encontrada:',
				intencoes.length > 1 ? 'Então, encontrei estas opções:' : 'Então, encontrei esta opção',
				intencoes.length > 1 ? 'Obrigado por aguardar, veja estas opções:' : 'Obrigado por aguardar, veja esta opção:',
				intencoes.length > 1 ? 'Identifiquei que estas opções podem lhe ajudar:' : 'Identifiquei que esta opção pode lhe ajudar:',
			]

			bot.getInstance().sendMessage(this.#mensagemTelegram.chat.id, mensagem[Math.floor(Math.random() * mensagem.length)], {
				reply_markup: { keyboard, one_time_keyboard: true }
			})
		} else {
			const prefixo = [
				'Não consegui entender, mas não vamos desistir né?',
				'Lamento, mas não entendi.',
				'Eita não entendi...',
				'Vamos tentar novamente pois não entendi como posso te ajudar.',
				'Acho que não entendi direito...',
			]

			const complemento = '\n\nDescreva com mais detalhes, ou com outras palavras, para eu te ajudar!'

			const mensagem = prefixo[Math.floor(Math.random() * prefixo.length)] + complemento

			bot.getInstance().sendMessage(this.#mensagemTelegram.chat.id, mensagem, {
				reply_markup: {
					keyboard: [[]],
					one_time_keyboard: true
				}
			})

		}

	}

}

module.exports = App