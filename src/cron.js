const cron = require('node-cron')
const notificacaoPromocaoDiaria = require('./services/NotificaPromocaoService');

cron.schedule('30 * * * * *', () => notificacaoPromocaoDiaria.notificar())