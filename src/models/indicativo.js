'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
	class Indicativo extends Model {

		static associate(models) {
			this.belongsTo(models.Intencao, {
				as: 'intencao',
				foreignKey: 'intencao_id'
			})
		}
	};
	Indicativo.init({
		nome: DataTypes.STRING,
		intencao_id: DataTypes.INTEGER
	}, {
		sequelize,
		modelName: 'Indicativo',
		tableName: 'indicativos',
		freezeTableName: true,
		underscored: true,
	});
	return Indicativo;
};