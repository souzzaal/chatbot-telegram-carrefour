'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
	class Mensagem extends Model {

		static associate(models) {
		}
	};

	Mensagem.init({
		chat_id: DataTypes.INTEGER,
		termos: DataTypes.STRING,
		intencao_id: DataTypes.INTEGER,
		util: DataTypes.INTEGER,
	}, {
		sequelize,
		modelName: 'Mensagem',
		tableName: 'mensagens',
		underscored: true,
		freezeTableName: true
	});

	return Mensagem;
};