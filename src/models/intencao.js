'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
	class Intencao extends Model {

		static associate(models) {
			this.hasMany(models.Indicativo, {
				as: 'indicativos',
				foreignKey: 'intencao_id'
			})
		}
	};
	Intencao.init({
		nome: DataTypes.STRING,
		ativo: {
			type: DataTypes.BOOLEAN,
			defaultValue: 1
		},
		aprende: DataTypes.BOOLEAN
	}, {
		sequelize,
		modelName: 'Intencao',
		tableName: 'intencoes',
		freezeTableName: true,
		underscored: true
	});
	return Intencao;
};