'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
	class NotificacaoTipo extends Model {

		static associate(models) {
		}
	};
	NotificacaoTipo.init({
		nome: DataTypes.STRING
	}, {
		sequelize,
		modelName: 'NotificacaoTipo',
		tableName: 'notificacoes_tipos',
		underscored: true,
	});
	return NotificacaoTipo;
};