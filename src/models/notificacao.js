'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
	class Notificacao extends Model {
		static associate(models) {
			this.belongsTo(models.NotificacaoTipo, {
				as: 'tipo',
				foreignKey: 'notificacao_tipo_id'
			})
		}
	};
	Notificacao.init({
		chat_id: DataTypes.INTEGER,
		notificacao_tipo_id: DataTypes.INTEGER
	}, {
		sequelize,
		modelName: 'Notificacao',
		tableName: 'notificacoes',
		underscored: true,
	});
	return Notificacao;
};