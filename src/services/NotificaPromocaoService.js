const bot = require('./BotService');
const { Notificacao, NotificacaoTipo } = require('../models');

class NotificaPromocaoService {

	async notificar() {

		const notificaoes = await Notificacao.findAll({
			include: [
				{
					model: NotificacaoTipo,
					as: 'tipo',
					where: {
						nome: 'promocao diaria'
					}
				}
			]
		})

		const semana = [
			'do Domingo',
			'da Segunda',
			'da Terça',
			'da Quarta',
			'da Quinta',
			'da Sexta',
			'do Sábado',
		]
		const mensagemInicial = `Confira as melhores promoções ${semana[new Date().getDay()]}`

		notificaoes.forEach(notificacao => {

			bot.getInstance().sendMessage(notificacao.chat_id, mensagemInicial)
		})

		console.log('Cron: promocões enviadas');
	}
}

module.exports = new NotificaPromocaoService()