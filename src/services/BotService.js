'use strict';

require('dotenv-safe').config({
	path: process.env.NODE_ENV ? `.env.${process.env.NODE_ENV}` : '.env'
})

const TelegramBot = require('node-telegram-bot-api')

class BotService {

	static #bot = null

	static getInstance() {
		if (this.#bot === null) {
			this.#bot = new TelegramBot(process.env.telegramBotToken, { polling: true });
			return this.#bot
		} else {
			return this.#bot
		}
	}
}

module.exports = BotService