const { Indicativo, Intencao, Mensagem } = require('../models')
const { Op } = require("sequelize");


class AprendizadoService {
	async salvaTermosPesquisados(chatId, termos) {
		await Mensagem.create({
			chat_id: chatId,
			termos: termos.join(' ')
		})
	}

	async salvaIntencao(chatId, intencaoTexto) {
		const intencao = await Intencao.findOne({
			where: { nome: intencaoTexto }
		})

		if (intencao && intencao.aprende) {
			const mensagem = await Mensagem.findOne({
				where: {
					chat_id: chatId
				},
				order: [['id', 'DESC']],
				limit: 1
			})


			if (mensagem && !mensagem.intencao_id) {
				await Mensagem.update({ intencao_id: intencao.id }, { where: { id: mensagem.id } })
			}
		}
	}

	async atualizaBaseDeConhecimento(chatId) {

		const mensagem = await Mensagem.findOne({
			where: {
				chat_id: chatId
			},
			order: [["id", 'DESC']],
			limit: 1
		})

		if (mensagem && mensagem.intencao_id > 0) {

			const termos = mensagem.termos.split(' ')

			for (let i = 0; i < termos.length; i++) {

				let indicativo = await Indicativo.findOrCreate({
					where: {
						nome: termos[i],
						intencao_id: mensagem.intencao_id
					}
				})
			}
		}

		await Mensagem.update({ util: 1 }, { where: { id: mensagem.id } })
	}


}

module.exports = new AprendizadoService()