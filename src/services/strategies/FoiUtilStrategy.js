const aprendizadoService = require('../AprendizadoService')
const BaseStrategy = require('./BaseStrategy')

class FoiUtilStrategy extends BaseStrategy {

	executar() {
		aprendizadoService.atualizaBaseDeConhecimento(this.getMensagemTelegram().chat.id)
		this.enviarMensagem('Obrigado!')
	}

}

module.exports = FoiUtilStrategy