const BaseStrategy = require('./BaseStrategy')


class LinhaDigitavelBoletoStratety extends BaseStrategy {

	executar() {

		this.enviarMensagem('Aqui está a linha digitável do seu boleto:')
		this.enviarMensagem('000000000000000000000000000000000000000000000000')
		this.enviarMensagemDeAvaliacao()

	}

}

module.exports = LinhaDigitavelBoletoStratety