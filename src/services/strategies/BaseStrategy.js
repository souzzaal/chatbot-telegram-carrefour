const aprendizadoService = require('../AprendizadoService')
const bot = require('../BotService')

class BaseStrategy {

	#bot = bot
	#mensagemTelegram = null

	constructor(mensagemTelegram) {
		this.#mensagemTelegram = mensagemTelegram
		aprendizadoService.salvaIntencao(mensagemTelegram.chat.id, mensagemTelegram.text)
	}

	executar() {
		throw new Error('O método executar() deve ser implementado')
	}

	enviarMensagem(mensagem, opcoes) {
		const chatId = this.#mensagemTelegram.chat.id
		this.#bot.getInstance().sendMessage(chatId, mensagem, opcoes)
	}

	getMensagemTelegram() {
		return this.#mensagemTelegram
	}

	enviarMensagemDeAvaliacao() {
		const opcoes = {
			reply_markup: {
				keyboard: [['/foiUtil'], ['/naoFoiUtil']],
				one_time_keyboard: true
			}
		}

		const mensagens = [
			'Como você avalia a solução apresentada?',
			'Por gentileza, o que me diz desta solução?',
			'Então, consegui te ajudar? A solução foi... ',
		]

		const mensagem = mensagens[Math.floor(Math.random() * mensagens.length)]

		this.enviarMensagem(mensagem, opcoes)
	}
}

module.exports = BaseStrategy