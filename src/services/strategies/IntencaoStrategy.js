const AderirPromocaoDiariaService = require('./AderirPromocaoDiariaService')
const CancelarPromocaoDiariaService = require('./CancelarPromocaoDiariaService')
const FoiUtilStrategy = require('./FoiUtilStrategy')
const LinhaDigitavelBoletoStrategy = require('./LinhaDigitavelBoletoStrategy')

class IntencaoStrategy {

	#intencao = null
	#strategy = null
	#mensagemTelegram = null

	constructor(intencao, mensagemTelegram) {
		this.#intencao = intencao
		this.#mensagemTelegram = mensagemTelegram
	}

	async executar() {
		this.identificarAcao()
		this.#strategy.executar()
	}

	identificarAcao() {
		switch (this.#intencao) {
			case '/linhaDigitavelBoleto':
				this.#strategy = new LinhaDigitavelBoletoStrategy(this.#mensagemTelegram)
				break;
			case '/foiUtil':
				this.#strategy = new FoiUtilStrategy(this.#mensagemTelegram)
				break;
			case '/receberPromocaoDiaria':
				this.#strategy = new AderirPromocaoDiariaService(this.#mensagemTelegram)
				break;
			case '/cancelarPromocaoDiaria':
				this.#strategy = new CancelarPromocaoDiariaService(this.#mensagemTelegram)
				break;

			default:
				break;
		}
	}

}

module.exports = IntencaoStrategy