const BaseStrategy = require('./BaseStrategy')
const { Notificacao, NotificacaoTipo } = require('../../models')

class CancelarPromocaoDiariaService extends BaseStrategy {

	async executar() {

		const notificacaoTipo = await NotificacaoTipo.findOne({
			where: {
				nome: 'promocao diaria'
			}
		})

		await Notificacao.destroy({
			where: {
				chat_id: this.getMensagemTelegram().chat.id,
				notificacao_tipo_id: notificacaoTipo.id
			}
		})

		this.enviarMensagem('Feito! Sua inscrição foi cancelada.')
	}

}

module.exports = CancelarPromocaoDiariaService