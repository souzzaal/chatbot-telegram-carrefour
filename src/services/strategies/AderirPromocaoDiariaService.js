const BaseStrategy = require('./BaseStrategy')
const { Notificacao, NotificacaoTipo } = require('../../models')

class AderirPromocaoDiariaService extends BaseStrategy {

	async executar() {
		const notificacaoTipo = await NotificacaoTipo.findOne({
			where: {
				nome: 'promocao diaria'
			}
		})

		await Notificacao.findOrCreate({
			where: {
				chat_id: this.getMensagemTelegram().chat.id,
				notificacao_tipo_id: notificacaoTipo.id
			}
		})

		this.enviarMensagem('Pronto! Agora você já está cadastrado para receber as nossas promoções. \n\nPara cancelar a inscrição me envie uma mensagem com /cancelarPromocaoDiaria')
	}

}

module.exports = AderirPromocaoDiariaService