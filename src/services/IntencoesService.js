const db = require('../models')
const { QueryTypes } = require('sequelize');
const IntencaoStrategy = require('./strategies/IntencaoStrategy')

class IntencoesService {

	async processaIntencao(intencao, mensagemTelegram) {
		const intencaoStrategy = new IntencaoStrategy(intencao, mensagemTelegram)
		intencaoStrategy.executar()
	}

	identificaTermosDePesquisa(fraseDoUsuario) {
		const termosDePesquisa = fraseDoUsuario
			.trim()
			.split(' ')
			.filter(palavra => palavra.length > 2)
			.map(palavra => palavra.toLocaleLowerCase().replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, ''))

		return termosDePesquisa
	}

	async identificaIntencao(fraseDoUsuario) {

		const termosDePesquisa = this.identificaTermosDePesquisa(fraseDoUsuario)

		const resposta = await db.sequelize.query(
			`
				SELECT
					i.nome, count(i.id) as relevancia 
				FROM intencoes i 
				INNER JOIN indicativos j ON j.intencao_id = i.id AND j.nome IN (:termos) 
				GROUP BY i.id 
				ORDER BY 2 DESC
			`
			,
			{
				replacements: { termos: termosDePesquisa },
				type: QueryTypes.SELECT,
				raw: true
			}
		)

		return resposta
	}
}

module.exports = new IntencoesService()