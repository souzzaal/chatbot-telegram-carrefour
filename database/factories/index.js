const faker = require('faker/locale/pt_BR')
const { factory } = require('factory-girl')
const { Intencao, Indicativo } = require('../../src/models')

factory.define('Intencao', Intencao, {
	nome: factory.sequence('intencao.nome', n => `#${faker.lorem.word()}${n}`) 	// nomes unicos
})

factory.define('Indicativo', Indicativo, {
	nome: faker.lorem.word,
	intencao_id: factory.assoc('Intencao', 'id')
})

module.exports = factory