'use strict';

const { Intencao } = require('../../src/models')

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await Intencao.findOrCreate({
			where: { nome: '/foiUtil' }
		})

		await Intencao.findOrCreate({
			where: { nome: '/naoFoiUtil' }
		})
	},

	down: async (queryInterface, Sequelize) => {
		await Intencao.destroy({
			where: { nome: '/foiUtil' }
		})

		await Intencao.destroy({
			where: { nome: '/naoFoiUtil' }
		})
	}
};
