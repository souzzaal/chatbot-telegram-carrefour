'use strict';
const { Op } = require("sequelize")
const { Intencao, Indicativo } = require('../../src/models')

module.exports = {
	up: async (queryInterface, Sequelize) => {

		const linhaDigitaval = await Intencao.findOne({
			where: { nome: '/linhaDigitavelBoleto' }
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'boleto',
				intencao_id: linhaDigitaval.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'linha',
				intencao_id: linhaDigitaval.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'digitavel',
				intencao_id: linhaDigitaval.id
			}
		})
	},

	down: async (queryInterface, Sequelize) => {

		const linhaDigitaval = await Intencao.findOne({
			where: { nome: '/linhaDigitavelBoleto' }
		})

		Indicativo.destroy({
			where: {
				intencao_id: linhaDigitaval.id
			}
		})
	}
};
