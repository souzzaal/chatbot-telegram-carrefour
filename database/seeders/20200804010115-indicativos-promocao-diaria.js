'use strict';
const { Op } = require("sequelize")
const { Intencao, Indicativo } = require('../../src/models')

module.exports = {
	up: async (queryInterface, Sequelize) => {

		const receberPromocaoDiaria = await Intencao.findOne({
			where: { nome: '/receberPromocaoDiaria' }
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'notificação',
				intencao_id: receberPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'notificacao',
				intencao_id: receberPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'alerta',
				intencao_id: receberPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'promoção',
				intencao_id: receberPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'promocao',
				intencao_id: receberPromocaoDiaria.id
			}
		})

		const cancelarPromocaoDiaria = await Intencao.findOne({
			where: { nome: '/cancelarPromocaoDiaria' }
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'notificação',
				intencao_id: cancelarPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'notificacao',
				intencao_id: cancelarPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'alerta',
				intencao_id: cancelarPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'promoção',
				intencao_id: cancelarPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'promocao',
				intencao_id: cancelarPromocaoDiaria.id
			}
		})

		await Indicativo.findOrCreate({
			where: {
				nome: 'cancelar',
				intencao_id: cancelarPromocaoDiaria.id
			}
		})

	},

	down: async (queryInterface, Sequelize) => {

		const receberPromocaoDiaria = await Intencao.findOne({
			where: { nome: '/receberPromocaoDiaria' }
		})

		Indicativo.destroy({
			where: {
				intencao_id: receberPromocaoDiaria.id
			}
		})

		const cancelarPromocaoDiaria = await Intencao.findOne({
			where: { nome: '/cancelarPromocaoDiaria' }
		})

		Indicativo.destroy({
			where: {
				intencao_id: cancelarPromocaoDiaria.id
			}
		})
	}
};
