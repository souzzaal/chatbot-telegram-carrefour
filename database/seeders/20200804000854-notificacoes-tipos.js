'use strict';
const { NotificacaoTipo } = require('../../src/models')

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await NotificacaoTipo.findOrCreate({
			where: { nome: 'promocao diaria' }
		})
	},

	down: async (queryInterface, Sequelize) => {
		await NotificacaoTipo.destroy({
			where: { nome: 'promocao diaria' }
		})

	}
};
