'use strict';

const { Intencao } = require('../../src/models')

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await Intencao.findOrCreate({
			where: { nome: '/receberPromocaoDiaria' },
			defaults: { aprende: 1 }
		})

		await Intencao.findOrCreate({
			where: { nome: '/cancelarPromocaoDiaria' },
			defaults: { aprende: 1 }
		})
	},

	down: async (queryInterface, Sequelize) => {
		await Intencao.destroy({
			where: { nome: '/receberPromocaoDiaria' }
		})

		await Intencao.destroy({
			where: { nome: '/cancelarPromocaoDiaria' }
		})
	}
};
