'use strict';

const { Intencao } = require('../../src/models')

module.exports = {
	up: async (queryInterface, Sequelize) => {
		const intencao = await Intencao.findOrCreate({
			where: { nome: '/linhaDigitavelBoleto' },
			defaults: { aprende: 1 }
		})
	},

	down: async (queryInterface, Sequelize) => {
		await Intencao.destroy({
			where: { nome: '/linhaDigitavelBoleto' }
		})
	}
};
