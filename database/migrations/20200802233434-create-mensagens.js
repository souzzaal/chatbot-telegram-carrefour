'use strict';
module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable('mensagens', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			chat_id: {
				type: Sequelize.INTEGER
			},
			termos: {
				type: Sequelize.STRING
			},
			intencao_id: {
				type: Sequelize.INTEGER,
				references: {
					model: 'intencoes',
					key: 'id'
				}
			},
			util: {
				type: Sequelize.BOOLEAN
			},
			created_at: {
				allowNull: false,
				type: Sequelize.DATE
			},
			updated_at: {
				allowNull: false,
				type: Sequelize.DATE
			}
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable('mensagens');
	}
};